#!/usr/bin/python3

import os

# start the ssh service so users within ansible nodes can connect to each other
# this is used by the jenkins build job to ssh into the enigma node and then run
# ansible-pull to install enigma
os.system('mkdir /run/sshd && /usr/sbin/sshd -D &')

# if there are playbooks specified; then run ansible-pull for those playbooks
git_repo = "https://gitlab.com/mark_gerken/ansible_devops_pipeline.git"
os.system(f'[ $playbooks ] && ansible-pull -U {git_repo} > /ansible_output $playbooks')

# create the flag file indicating this container is up and running
open('/ansible_client_started.flag', 'x')

# tail /dev/null to keep the container running
os.system("tail -f /dev/null")
